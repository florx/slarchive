<?php

class MessageParser {

	private $templateEngine;
	private $emojiData;
	private $message;
	private $previousMessage;
	private $users;
	private $channels;

	private $warningFunctionMissingCache = array();

	/**
	 * Sets up the MessageParser, and obtains the Emoji Data from the json file.
	 * @param TemplateEngine $templateEngine
	 */
	public function __construct($templateEngine) {
		$this->templateEngine = $templateEngine;
		$this->emojiData = json_decode(file_get_contents(__DIR__ . '/emoji.json'));
	}

	public function setData($users, $channels) {
		$this->users = $users;
		$this->channels = $channels;
	}

	/**
	 * Optionally shows the date divider. Will only show if the date has changed from the previous message.
	 *
	 * Looks like ---- March 16th 2016 ----
	 *
	 * @param stdClass $message
	 * @param String $lastDateDivider
	 * @return String (html)
	 */
	public function showDateDivider($message, $lastDateDivider) {

        $html = '';

		$dateDivider = date('M jS Y', $message->ts);

		if ($dateDivider != $lastDateDivider) {
			$lastDateDivider = $dateDivider;
			$html = $this->templateEngine->parse('date_divider', compact('dateDivider'));
		}

		return ['html' => $html, 'lastDateDivider' => $lastDateDivider];

	}

	/**
	 * Parses the message event, and works out what subtype it is.
	 *
	 * If the subtype method has been created, it overrides the default behaviour of the handler.
	 * e.g. if the subtype is bot_message, it will look for a method: parse_bot_message and use this to parse instead.
	 *
	 * @param stdClass $message
	 * @param stdClass $previousMessage
	 * @return String (html)
	 */
	public function parse($message, $previousMessage) {
		$this->message = $message;
		$this->previousMessage = $previousMessage;

		$subtype = isset($this->message->subtype) ? $this->message->subtype : "normal";

		if ($subtype != 'normal') {

			$functionName = "parse_" . $subtype;
			if (method_exists($this, $functionName)) {
				return $this->$functionName();
			} else {
				if (!in_array($functionName, $this->warningFunctionMissingCache)) {
					logger(WARNING, "Function " . $functionName . " doesn't exist. Parsing as a normal message.");
					$this->warningFunctionMissingCache[] = $functionName;
				}
			}
		}

		return $this->parse_normal();

	}

	/**
	 * The majority of messages will be of a "normal" subtype (e.g. they don't have a subtype)
	 *
	 * This is the default handler if it hasn't been overridden by creating a method "parse_$subtype"
	 *
	 * @return String (html)
	 */
	private function parse_normal() {
		return $this->buildMessage();
	}

	/**
	 * Overriding Pinned Item, as I get an event with item_type of 'F' that doesn't contain any text...
	 *
	 * @return String (html)
	 */
	private function parse_pinned_item() {

		if ($this->message->item_type == 'F') {
			//I assume this means removing a pinned item
			$this->message->text = $this->getUserRealName($this->message->user) . ' removed a pinned item';
		}

		return $this->buildMessage();
	}

	/**
	 * Handle File Comments if there is no user for the message
	 *
	 * @return String (html)
	 */
	private function parse_file_comment() {
		if (!isset($this->message->user)) {
			return '';
		}

		return buildMessage($this->message);
	}

	/**
	 * Handle bot messages, as they look completely different to normal user messages
	 *
	 * @return String (html)
	 */
	private function parse_bot_message() {

		if (isset($this->message->text) && !empty($this->message->text)) {
			$text = $this->message->text;
		} elseif (isset($this->message->attachments)) {
			$text = '<em>This message has an attachment</em>';
		} else {
			$text = '<em>This message has no text</em>';
		}

		if (isset($this->message->icons)) {
			$user_image = reset($this->message->icons);
		}
		//just get the first one
		else {
			$user_image = '';
		}

		if (!isset($this->message->username) && isset($this->message->bot_id)) {
			$this->message->username = $this->message->bot_id;
		}

		$user_realname = $this->message->username;
		$time = date('Y-m-d H:i', $this->message->ts);
		$reactions = $this->buildReactions();

		return $this->templateEngine->parse('message_full', compact('text', 'reactions', 'time', 'timestamp', 'user_image', 'user_realname'));

	}

	/**
	 * This is the main message builder, most message events will end up here.
	 *
	 * It will automatically parse @user, @everyone, @channel, #channel, links and emoji
	 * It also builds the emoji reactions and will group together messages if they're from the same user, and within 5 minutes
	 *
	 * @return String (html)
	 */
	private function buildMessage() {

		$text = $this->message->text;

		//neaten up the text
		$text = nl2br(trim($text));

		//This handles @userId messages, these can appear anywhere in text.
		$text = $this->subUserAts($text);

		//This handles @userId|username
		$text = $this->subComplexUserAts($text);

		//This handles and #channel messages, these can appear anywhere in text.
		$text = $this->subHashtagChannels($text);

		//This handles links
		$text = $this->subLinks($text);

		//handle <!channel> or <!everyone> to show BOLD @channel
		$text = $this->subChannelNotifications($text);

		//this handles emoji
		$text = $this->subEmojis($text);

		$reactions = $this->buildReactions();

		//this groups up messages if the user of this message and the previous one is the same
		//AND if they are less than 5 minutes apart
		if ($this->previousMessage != null && isset($this->previousMessage->user) && isset($this->message->user) &&
			$this->previousMessage->user == $this->message->user && ($this->message->ts - $this->previousMessage->ts < 900)) {
			return $this->templateEngine->parse('message_tiny', compact('text', 'reactions'));
		}

		$user_image = $this->getUserImage($this->message->user);
		$user_realname = $this->getUserRealName($this->message->user);
		$time = date('Y-m-d H:i', $this->message->ts);
		$timestamp = $this->message->ts;

		return $this->templateEngine->parse('message_full', compact('text', 'reactions', 'time', 'timestamp', 'user_image', 'user_realname'));
	}

	/**
	 * Gets the user object for a specific userId
	 *
	 * @param String $userId
	 * @return stdClass
	 */
	private function getUser($userId) {
		if (isset($this->users[$userId])) {
			return $this->users[$userId];
		}

		return null;
	}

	/**
	 * Gets the user image for a specific userId
	 *
	 * @param String $userId
	 * @return String
	 */
	private function getUserImage($userId) {
		$user = $this->getUser($userId);

		if (!$user) {
			return '';
		}

		return $user->profile->image_72;
	}

	/**
	 * Gets the real name for a specific userId
	 *
	 * @param String $userId
	 * @return String
	 */
	private function getUserRealName($userId) {
		$user = $this->getUser($userId);

		if (!$user) {
			return '';
		}

		return $user->profile->real_name;
	}

	/**
	 * Gets the username for a specific userId
	 *
	 * @param String $userId
	 * @return String
	 */
	private function getUsername($userId) {
		$user = $this->getUser($userId);

		if (!$user) {
			return '';
		}

		return $user->name;
	}

	/**
	 * Gets the channel name for a specific channelId
	 *
	 * @param String $channelId
	 * @return String
	 */
	private function getChannelName($channelId) {

		if (isset($this->channels[$channelId])) {
			return $this->channels[$channelId]->name;
		}

		return 'Unkown Channel - ' . $channelId;
	}

	/**
	 * Builds the reaction html
	 *
	 * @return type
	 */
	private function buildReactions() {
		if (!isset($this->message->reactions)) {
			return '';
		}

		$html = '';

		foreach ($this->message->reactions as $reaction) {

			$namesList = array();

			foreach ($reaction->users as $user) {
				$namesList[] = $this->getUserRealName($user);
			}

			$names = join(', ', $namesList);
			$emoji = $this->parseEmoji($reaction->name, false);
			$count = $reaction->count;

			$html .= $this->templateEngine->parse('reaction', compact('names', 'emoji', 'count'));
		}

		return $this->templateEngine->parse('reactions', compact('html'));
	}

	/**
	 * Handles any simple @user, they come in the format <@U00000000>
	 *
	 * @param String $text
	 * @return String
	 */
	private function subUserAts($text) {
		if (strpos($text, '@') !== FALSE) {
			$re = "/<@(U[0-9a-z]+)>/i";
			if (preg_match_all($re, $text, $matches)) {

				for ($i = 0; $i < count($matches[0]); $i++) {
					$userId = $matches[1][$i];
					$replaceString = $matches[0][$i];

					$username = $this->getUsername($userId);
					$replacement = $this->templateEngine->parse('snippet_at', compact('username'));

					$text = str_replace($replaceString, $replacement, $text);
				}
			}
		}
		return $text;
	}

	/**
	 * Handles any complex @user, they come in the format <@U00000000|username>
	 *
	 * @param String $text
	 * @return String
	 */
	private function subComplexUserAts($text) {
		if (strpos($text, '@') !== FALSE) {
			$re = "/<@(U[0-9a-z]+)\|[a-z0-9._]+>/i";
			if (preg_match_all($re, $text, $matches)) {

				for ($i = 0; $i < count($matches[0]); $i++) {
					$userId = $matches[1][$i];
					$replaceString = $matches[0][$i];

					$userRealname = $this->getUserRealName($userId);
					//$replacement = $this->templateEngine->parse('snippet_at', compact('username'));

					$text = str_replace($replaceString, $userRealname, $text);
				}
			}
		}
		return $text;
	}

	/**
	 * Handles any #channel, they come in the format <#C00000000>
	 *
	 * @param String $text
	 * @return String
	 */
	private function subHashtagChannels($text) {
		if (strpos($text, '#') !== FALSE) {
			$re = "/<#(C[0-9a-z]+)>/i";
			if (preg_match_all($re, $text, $matches)) {

				for ($i = 0; $i < count($matches[0]); $i++) {
					$channelId = $matches[1][$i];
					$replaceString = $matches[0][$i];

					$channelName = $this->getChannelName($channelId);
					$replacement = $this->templateEngine->parse('snippet_hash', compact('channelName'));

					$text = str_replace($replaceString, $replacement, $text);
				}
			}
		}
		return $text;
	}

	/**
	 * Handles any links, they come in the format <http://www.example.com>
	 *
	 * @param String $text
	 * @return String
	 */
	private function subLinks($text) {
		if (stripos($text, '<http') !== false) {
			$linksInMessage = explode('<http', $text);
			foreach ($linksInMessage as $linkInMessage) {
				$array = explode('>', $linkInMessage);
				$linkTotalInBrackets = $array[0];
				$array = explode('|', $array[0]);
				$linkInMessage = $array[0];
				$text = str_replace('<http' . $linkTotalInBrackets . '>', '<a href="http' . $linkInMessage . '">http' . $linkInMessage . '</a>', $text);
			}
		}
		return $text;
	}

	/**
	 * Handles any @everyone or @channel, they come in the format <!everyone> or <!channel>
	 * 
	 * @param String $text 
	 * @return String
	 */
	private function subChannelNotifications($text) {
		if (strpos($text, '<!') !== FALSE) {
			$re = "/<!([0-9a-z]+)>/i";
			if (preg_match_all($re, $text, $matches)) {

				for ($i = 0; $i < count($matches[0]); $i++) {
					$name = $matches[1][$i];
					$replaceString = $matches[0][$i];

					$replacement = $this->templateEngine->parse('snippet_at', array('username' => $name));

					$text = str_replace($replaceString, $replacement, $text);
				}
			}
		}
		return $text;
	}

	/**
	 * Handles any emoji, they come in the format :emoji:
	 * 
	 * @param String $text 
	 * @return String
	 */
	private function subEmojis($text) {
		if (strpos($text, ':') !== FALSE) {

			$re = "/:([0-9a-z_\-]{3,}|\+1|\-1):/i";
			if (preg_match_all($re, $text, $matches)) {

				for ($i = 0; $i < count($matches[0]); $i++) {
					$emojiId = $matches[1][$i];
					$replaceString = $matches[0][$i];

					$text = str_replace($replaceString, $this->parseEmoji($emojiId), $text);
				}
			}
		}
		return $text;
	}

	/**
	 * Parses the emoji to find the correct location on the emoji sprite image.
	 * 
	 * @param String $text 
	 * @return String
	 */
	private function parseEmoji($emojiId, $showTitle = true) {

		$title = $showTitle ? 'title="' . $emojiId . '"' : '';
		$size = 41;

		//hack to get it to display a face
		if ($emojiId == 'simple_smile') {
			return $this->templateEngine->parse('emoji_tiny', compact('emojiId', 'title'));
		}

		if (isset($this->emojiData->$emojiId)) {

			$mul = 100 / ($size - 1);
			$x = ($mul * $this->emojiData->$emojiId->x);
			$y = ($mul * $this->emojiData->$emojiId->y);

			return $this->templateEngine->parse('emoji_full', compact('emojiId', 'title', 'size', 'x', 'y'));
		}

		return ':' . $emojiId . ':';

	}

}