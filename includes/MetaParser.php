<?php

class MetaParser {

	/**
	 * Combines all of the channel json date files into one master channel json file.
	 *
	 * @param String $archiveDir
	 * @param String $jsonDir
	 */
	public function combineChannelJson($archiveDir, $jsonDir) {

		$files = scandir($archiveDir);

		foreach ($files as $channelFoldername) {

			if ($channelFoldername == '.' || $channelFoldername == '..') {
				continue;
			}

			if (is_dir($archiveDir . $channelFoldername)) {
				$channelJsonFile = $jsonDir . $channelFoldername . '.json';
				if (file_exists($channelJsonFile)) {
					logger(WARNING, "JSON already exists " . $channelJsonFile);
				}

				unset($chats);
				$chats = array();

				logger(INFO, 'Combining JSON files for #' . $channelFoldername);

				$dates = scandir($archiveDir . $channelFoldername);
				foreach ($dates as $date) {
					if (!is_dir($date)) {
						$messages = json_decode(file_get_contents($archiveDir . $channelFoldername . '/' . $date), true);
						if (empty($messages)) {
							continue;
						}

						foreach ($messages as $message) {
							array_push($chats, $message);
						}
					}
				}

				file_put_contents($channelJsonFile, json_encode($chats));
				logger(INFO, number_format(count($chats)) . ' messages exported to ' . $channelJsonFile);
			}
		}

	}

	public function parseUsers($archiveDir) {

        if(!is_file($archiveDir . 'users.json')){
            exit('Cannot find the users.json file in "' . $archiveDir . 'users.json' . '". Did you unzip the Slack archive to this folder?');
        }

		$users = json_decode(file_get_contents($archiveDir . 'users.json'));
		$usersById = array();
		foreach ($users as $user) {
			$usersById[$user->id] = $user;
		}

		return $usersById;
	}

	public function parseChannels($archiveDir) {
        if(!is_file($archiveDir . 'channels.json')){
            exit('Cannot find the users.json file in "' . $archiveDir . 'channels.json' . '". Did you unzip the Slack archive to this folder?');
        }

		$channels = json_decode(file_get_contents($archiveDir . 'channels.json'));
		$channelsById = array();
		foreach ($channels as $channel) {
			$channelsById[$channel->id] = $channel;
		}

		return $channelsById;
	}

}