<?php

class TemplateEngine {

	private $templateDir;
	private $loadedTemplates = array();

	public function __construct($templateDir) {
		$this->templateDir = $templateDir;

	}

	/**
	 * Loads and parses a template file using the simple {{variable}} convention.
	 *
	 * @param String $templateName
	 * @param array $variables
	 * @return String (html)
	 */
	public function parse($templateName, $variables) {

		//Check if the template has already been loaded to save going to disk again.
		if (in_array($templateName, $this->loadedTemplates)) {
			$template = $this->loadedTemplates[$templateName];
		} else {
			$template = file_get_contents($this->templateDir . $templateName . '.html');
			$this->loadedTemplates[$templateName] = $template;
		}

		//Replace all the variables we have been given
		foreach ($variables as $key => $value) {
			$template = str_replace('{{' . $key . '}}', $value, $template);
		}

		return $template;

	}

}