<?php

class SlackParser {

	private $templateEngine;
	private $metaParser;

	private $baseDir;
	private $archiveDir;
	private $jsonDir;
	private $htmlDir;
	private $users;
	private $channels;

	/**
	 * Setup all the directory properties
	 */
	public function __construct() {
		$this->templateEngine = new TemplateEngine(__DIR__ . '/templates/');
		$this->metaParser = new MetaParser();
		$this->msgParser = new MessageParser($this->templateEngine);

		$this->baseDir = __DIR__ . '/../';
		$this->archiveDir = $this->baseDir . 'archive/';
		$this->jsonDir = $this->baseDir . 'json/';
		$this->htmlDir = $this->baseDir . 'html/';
		$this->htmlDebugDir = $this->baseDir . 'html/debug/';

	}

	/**
	 * The meat of the operation, makes directories, manages the meta parser and then kicks off the channel runner.
	 */
	public function run() {
		$this->makeDirectories();

		$this->metaParser->combineChannelJson($this->archiveDir, $this->jsonDir);
		$this->users = $this->metaParser->parseUsers($this->archiveDir);
		$this->channels = $this->metaParser->parseChannels($this->archiveDir);

		$this->msgParser->setData($this->users, $this->channels);

		$this->channelRunner();

		$this->makeIndex();

	}

	/**
	 * Does what it says on the tin, makes any directories that don't already exist.
	 */
	private function makeDirectories() {
		if (!is_dir($this->jsonDir)) {
			mkdir($this->jsonDir);
		}

		if (!is_dir($this->htmlDir)) {
			mkdir($this->htmlDir);
		}

		if (!is_dir($this->htmlDebugDir) && DEBUG) {
			mkdir($this->htmlDebugDir);
		}

	}

	/**
	 * Loops though each of the channels looking for messags to parse.
	 */
	private function channelRunner() {

		$channels = scandir($this->jsonDir);

		foreach ($channels as $channelFilename) {

			if ($channelFilename == '.' || $channelFilename == '..') {
				continue;
			}

			if (is_dir($channelFilename)) {
				continue;
			}

			//remove .json from the filename
			$channelName = substr($channelFilename, 0, -5);
			$lastDateDivider = '';
			$html = '';
			$previousMessage = null;

			logger(INFO, 'Parsing messages for ' . $channelName);
			$messages = json_decode(file_get_contents($this->jsonDir . $channelFilename));

			foreach ($messages as $message) {
				//Date divider e.g. ----- April 3rd 2016 ------
				$rtnDateDivider = $this->msgParser->showDateDivider($message, $lastDateDivider);
                $html .= $rtnDateDivider['html'];
                $lastDateDivider = $rtnDateDivider['lastDateDivider'];

				//Parse message
				$html .= $this->msgParser->parse($message, $previousMessage);
				$previousMessage = $message;
			}

			$page = $this->templateEngine->parse('channel_view', compact('html'));
			file_put_contents($this->htmlDir . $channelName . '.html', $page);

		}

	}

	/**
	 * Creates the index.html file in the root so users can easily navigate to the channels in the /html folder
	 */
	private function makeIndex(){

		$html = '';

		foreach($this->channels as $channel){
			$channelName = $channel->name;

			$html .= $this->templateEngine->parse('snippet_hash', compact('channelName')) . '<br />';
		}

		file_put_contents($this->baseDir . 'index.html', $this->templateEngine->parse('index_view', compact('html')));

	}

}