<?php

ini_set('memory_limit', '1024M');
date_default_timezone_set('UTC');
mb_internal_encoding("UTF-8");

define("INFO", 0);
define("WARNING", 1);
define("ERROR", 2);
define("DEBUG", 0); //set to 1 to output to a /debug folder with HTML comments containing the full JSON of the message

spl_autoload_register(function ($class) {
    include __DIR__ . '/' . $class . '.php';
});

function logger($type, $message){
	$style = "";
	$typeStr = '';
	if($type == INFO){
		$style = "0;34";
		$typeStr = "INFO";
	}else if($type == WARNING){
		$style = "1;33";
		$typeStr = "WARNING";
	}else if($type == ERROR){
		$style = "0;31";
		$typeStr = "ERROR";
	}

	echo date('Y-m-d H:i:s') . " \033[{$style}m [".$typeStr."] ".$message."\033[0m\n";
}