# Slack Archive Parser #

This will parse a Slack JSON archive, and make it look pretty and easy to read.

### How do I get set up? ###

* git clone
* Download your archive from http://my.slack.com/services/export
* Unzip to the root of the /archive folder (you should see /archive/users.json and /archive/channels.json plus a load of folders named after your channels if you've done it correctly)
* chmod 0777 for the containing folder of the project
* php run.php
* Look at index.html